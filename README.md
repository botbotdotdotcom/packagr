
# packagr - alpha

<!-- badges: start -->

<!-- badges: end -->

<!-- README.md is generated from README.Rmd. Please edit that file -->

The goal of packagr is to simplify package management for R

## Installation

``` r
# install.packages("devtools") # if you have not installed "devtools" package
devtools::install_git('https://gitlab.com/botbotdotdotcom/packagr')
library(packagr)
```

<!-- ## TODO: Installation
``` r
install.packages("packagr")
```-->

## Example

``` r
packages <- c('beepr','readr','tidyr')
packagr(packages)
```

## TODO:

#### FEATURES

  - [ ] Check `rProfile` in rStudio for defaults
    <!--# > Sys.getenv("R_HOME") 
    # > ?Startup
    # > Sys.getenv("HOME")
    # [see](https://support.rstudio.com/hc/en-us/community/posts/200635318-loading-Rprofile-site-settings-from-Rstudio)-->
      - [ ] Then export list of defaults to copy into page
  - [ ] Set my default favourite packages
  - [ ] How long it took to execute
  - [ ] Show instructions for resolving package conflicts
  - [ ] CLI to bootstrapping R package
  - [ ] Addin to search plugins from rStudio, VS Code, Atom, Sublime,
    etc
  - [ ] Simple update R packages

#### TESTS:

  - [ ] Add and write tests
  - [ ] `library(lintr) lintr::lint_package("~/gitlab/packagr")`
  - [ ] Plural `package` -\> packages
  - [x] tryCatch
  - [x] Check works for single package

#### INITIAL SETUP

  - [ ] [Orcid](https://orcid.org/)
  - [x] [use this](https://github.com/r-lib/usethis)
  - [x] how to access script before package done:

<!--
```R
start.time <- Sys.time()
...Relevent codes...
end.time <- Sys.time()
time.taken <- end.time - start.time
time.taken
```-->

<!--### Reference:
- https://www.one-tab.com/page/TdZs-kkfSvql1pEwP9ujkA <- best list
- Guide to creating package with Gitlab CI:
  - [pt1](https://blog.methodsconsultants.com/posts/developing-r-packages-using-gitlab-ci-part-i/)
  - [pt2](https://blog.methodsconsultants.com/posts/developing-r-packages-with-usethis-and-gitlab-ci-part-ii/)
  - [ ] go through [pt3](https://blog.methodsconsultants.com/posts/developing-r-packages-with-usethis-and-gitlab-ci-part-iii/)
- Old guide without usethis:
  - http://tinyheero.github.io/jekyll/update/2015/07/26/making-your-first-R-package.html
```bash
warning
✔ Adding 'Rcpp' to Imports field in DESCRIPTION
Warning messages:
1: In if (delta < 0) { :
  the condition has length > 1 and only the first element will be used
Calls: <Anonymous> -> use_dependency
2: In if (delta > 0) { :
  the condition has length > 1 and only the first element will be used
Calls: <Anonymous> -> use_dependency
```
❯ checking Rd metadata ... WARNING
  Rd files with duplicated alias 'packagr':
    ‘packagr-package.Rd’ ‘packagr.Rd’
❯ checking Rd \usage sections ... WARNING
  Undocumented arguments in documentation object 'packagr'
    ‘package’ ‘...’
  Documented arguments not in \usage in documentation object 'packagr':
    ‘var’
  Functions with \usage entries need to have the appropriate \alias entries, and all their arguments documented.
  The \usage entries must correspond to syntactically valid R code.
  See chapter ‘Writing R documentation files’ in the ‘Writing R Extensions’ manual.
❯ checking DESCRIPTION meta-information ... NOTE
  Malformed Description field: should contain one or more complete sentences.
❯ checking R code for possible problems ... NOTE
  packagr: no visible global function definition for ‘installed.packages’
  packagr: no visible global function definition for ‘install.packages’
  Undefined global functions or variables:
    install.packages installed.packages
  Consider adding
    importFrom("utils", "install.packages", "installed.packages")
  to your NAMESPACE file.
```-->
